module.exports = function(grunt) {

  // because why not
  "use strict";

  // 1. All configuration goes here
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),

    devUpdate: {
    	main: {
    	 options: {
      	  reportUpdated: false,
          updateType: "prompt",
          semver: false
        }
      }
    },

    sass: {
      global: {
        options: {
          style: "compressed",
          precision: 10
        },
        files: {
          "assets/css/flexslider-unprefixed.css": "assets/scss/flexslider.scss"
        }
      }
    },

    autoprefixer: {
      global: {
        src: "assets/css/flexslider-unprefixed.css",
        dest: "assets/css/flexslider.css"
      }
    },

    watch: {
      options: {
        spawn: false
      },
      css: {
        files: ["assets/scss/*.scss"],
        tasks: ["sass", "autoprefixer"]
      },
      compression: {
        files: ['assets/**/*', 'language/**/*', 'tmpl/**/*', 'helper.php', 'mod_reslider.php', 'mod_reslider.xml'],
        tasks: ["compress"]
      }
    },

    compress: {
      main: {
        options: {
          archive: "mod_reslider-x-x-x.zip",
          pretty: true,
          mode: 'zip'
        },
        files: [
          {expand: true, src: ['assets/css/flexslider.css'], dest: '/'},
          {expand: true, src: ['assets/css/index.html'], dest: '/'},
          {expand: true, src: ['assets/js/jquery.flexslider-min.js'], dest: '/'},
          {expand: true, src: ['assets/js/no-conflict.js'], dest: '/'},
          {expand: true, src: ['assets/js/index.html'], dest: '/'},
          {expand: true, src: ['assets/images/bg_direction_nav.png'], dest: '/'},
          {expand: true, src: ['language/**'], dest: '/'},
          {expand: true, src: ['tmpl/**'], dest: '/'},
          {expand: true, src: ['helper.php'], dest: '/'},
          {expand: true, src: ['index.html'], dest: '/'},
          {expand: true, src: ['mod_reslider.php'], dest: '/'},
          {expand: true, src: ['mod_reslider.xml'], dest: '/'},
        ]
      }
    }
});

// 3. Where we tell Grunt we plan to use this plug-in.
require("load-grunt-tasks")(grunt);

// 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
grunt.registerTask("default", ["sass", "autoprefixer", "compress", "watch"]);

};
